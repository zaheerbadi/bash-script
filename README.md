# Switch PHP Version Script

## Overview

This bash script simplifies the process of switching PHP versions in a Ubuntu 22.04 LTS environment running Apache with FPM (FastCGI Process Manager). It addresses the challenge of ensuring that changes made using `update-alternatives` for the PHP CLI version are reflected in Apache when using PHP FPM.

## Features

- **Dynamic Switching:** The script dynamically switches PHP versions for both CLI and Apache with FPM.
- **Efficiency:** Eliminates the need for multiple commands, making PHP version switching more efficient.
- **Compatibility:** Works with multiple PHP versions (e.g., 8.1, 8.2, 8.3).

## Usage Guide

### 1. Download the Script

```bash
wget https://gitlab.com/zaheerbadi/bash-script/-/blob/c09256c81b02e864e38b96bd2bd327dff26b1d65/switch_php_version.sh
```

### 2. Make it Executable

```bash
chmod +x switch_php_version.sh
```

### 3. Move to a Directory in PATH

```bash
mv switch_php_version.sh ~/bin/
```

### 4. Update Shell Configuration

Add the following line to your shell configuration file (e.g., `~/.bashrc` or `~/.zshrc`):

```bash
export PATH="$HOME/bin:$PATH"
```

### 5. Reload Shell Configuration

```bash
source ~/.bashrc
```

### 6. Run the Script Anywhere

```bash
switch_php_version.sh
```

## Support and Contribution

For support or contributions, feel free to open an issue or pull request on the [GitHub repository](https://gitlab.com/zaheerbadi/bash-script).

## License

This project is licensed under the [MIT License](LICENSE).

## Project Status

Development is actively ongoing. Contributions and feedback are welcome!
