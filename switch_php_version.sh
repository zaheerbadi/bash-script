#!/bin/bash

# Function to disable previous PHP-FPM conf and enable the new one
switch_php_version() {
    local previous_version="$1"
    local new_version="$2"

    sudo a2disconf "php${previous_version}-fpm"
    sudo a2enconf "php${new_version}-fpm"
    sudo systemctl restart apache2
}

# Function to switch PHP CLI version
switch_cli_version() {
    local version="$1"
    sudo update-alternatives --set php "/usr/bin/php${version}"
}

# Display available PHP versions
echo "Available PHP versions:"
sudo update-alternatives --list php

# Guide for PHP versions
echo -e "\nGuide:"
echo "1: PHP 8.1"
echo "2: PHP 8.2"
# Add more versions if needed

# Ask user for the desired PHP version
read -p "Enter the number corresponding to the PHP version you want to switch to: " php_choice

case $php_choice in
    1)
        php_version="8.1"
        ;;
    2)
        php_version="8.2"
        ;;
    # Add more cases if needed
    *)
        echo "Invalid choice. Exiting."
        exit 1
        ;;
esac

# Get the current PHP version
current_version=$(php -v | grep -oP 'PHP \K[0-9]+\.[0-9]+')

# Switch PHP version for CLI
switch_cli_version "$php_version"

# Switch PHP version for Apache PHP-FPM
switch_php_version "$current_version" "$php_version"

echo "PHP version switched to $php_version for both CLI and Apache."
